<?php
use App\Helpers\BibClass;
use App\Helpers\BibDB;
use Illuminate\Support\Facades\DB;
function sidebarLink($title, $link = "", $icon = "")
{
?>
    <li class="nk-menu-item">
        <a href="<?php echo $link; ?>" class="nk-menu-link ">
            <?php if ($icon != "") : ?> <span class="nk-menu-icon"><em class="icon ni ni-<?php echo $icon; ?>"></em></span><?php endif; ?>
            <span class="nk-menu-text"><?php label($title); ?></span>
        </a>
    </li>
<?php
}
//Action Buttons
//We can apply auth in below functions
function rowActionView($url)
{
    echo "<a href=\"$url\" type=\"button\" class=\"btn btn-color-primary btn-hover-primary btn-icon btn-soft\" ><em class=\"icon ni ni-eye\"></em></a>";
}
function rowActionEdit($url)
{
    echo "<a href=\"$url\" type=\"button\" class=\"btn btn-color-info btn-hover-info btn-icon btn-soft\" ><em class=\"icon ni ni-edit\"></em></a>";
}
function rowActionButton($buttonName, $url = "", $iconClass = "ni-edit")
{
    echo "<a href=\"$url\" type=\"button\" class=\"btn btn-color-success btn-hover-success btn-icon btn-soft\" title=\"$buttonName\" ><em class=\"icon ni $iconClass\"></em></a>";
}
function getAddressCompiled($address_id)
{
    $t = "SELECT tbl_address.wardNo, tbl_address.cityName, (select tbl_municipality.municipalityName from tbl_municipality where tbl_municipality.municipality_id=tbl_address.muncipality_id) as muncipalityName, (select tbl_district.districtName from tbl_district where tbl_district.district_id=tbl_address.district_id) as districtName, (select tbl_state.stateName from tbl_state WHERE tbl_state.state_id=tbl_address.province_id) as provinceName, (select tbl_country.countryName from tbl_country where tbl_country.country_id=tbl_address.address_id) as countryName FROM `tbl_address` WHERE address_id='$address_id'";


    $AddressRow = BibDB::getRowByQuery($t);
    if ($AddressRow)
        return " $AddressRow->cityName, Ward $AddressRow->wardNo, $AddressRow->muncipalityName, $AddressRow->districtName, $AddressRow->provinceName, $AddressRow->countryName ";
    else
        return false;
}
function getIndustries($condition = "1=1")
{
    $user_id = auth()->user()->id;
    $t = "select *,
    (select CONCAT(firstName,' ',middleName,' ',lastName) from tbl_proprietor where tbl_proprietor.proprietor_id =tbl_industry.proprietor_id ) as proprietorName,
    (select countryName from tbl_country where tbl_country.country_id=(select country_id from tbl_address where tbl_address.address_id=tbl_industry.address_id)) as country,
   (select stateName from tbl_state where tbl_state.state_id=(select province_id from tbl_address where tbl_address.address_id=tbl_industry.address_id)) as state,
   (select districtName from tbl_district where tbl_district.district_id = (select district_id from tbl_address where tbl_address.address_id=tbl_industry.address_id)) as district,
   (select municipalityName from tbl_municipality where tbl_municipality.municipality_id = (select muncipality_id from tbl_address where tbl_address.address_id=tbl_industry.address_id)) as municipalityName,
   (select wardNo from tbl_address where tbl_address.address_id=tbl_industry.address_id) as wardNo,
   (select cityName from tbl_address where tbl_address.address_id=tbl_industry.address_id) as cityName
    from tbl_industry where $condition AND createdBy = $user_id AND status !=-1";
    $DataRow = BibDB::getTableByQuery($t);
    if ($DataRow) {
        return $DataRow;
    } else return false;
}
//for checking submitted document or not


// select submittedDocuments from tbl_recommendationapplications where tbl_industry.createdBy=tbl_recommendationapplications.createdBy) as submittedDocuemnts
function getIndustryCompiled($industry_id)
{
    $t = "select *, (select countryName from tbl_country where tbl_country.country_id=(select country_id from tbl_address where tbl_address.address_id=tbl_industry.address_id)) as country,
   (select stateName from tbl_state where tbl_state.state_id=(select province_id from tbl_address where tbl_address.address_id=tbl_industry.address_id)) as state,
   (select districtName from tbl_district where tbl_district.district_id = (select district_id from tbl_address where tbl_address.address_id=tbl_industry.address_id)) as district,
   (select municipalityName from tbl_municipality where tbl_municipality.municipality_id = (select muncipality_id from tbl_address where tbl_address.address_id=tbl_industry.address_id)) as municipalityName,
   (select wardNo from tbl_address where tbl_address.address_id=tbl_industry.address_id) as wardNo,
   (select cityName from tbl_address where tbl_address.address_id=tbl_industry.address_id) as cityName
    from tbl_industry where industry_id=$industry_id";
    $DataRow = BibDB::getRowByQuery($t);
    if ($DataRow) {
        $DataRow->FullAddress=getAddressCompiled($DataRow->address_id);
        $DataRow->Proprietor = getApplicantCompiled($DataRow->proprietor_id);
        $DataRow->Partners = getPartners($DataRow->industry_id);
        return $DataRow;
    } else
        return false;
}
function getProprietor($proprietor_id)
{
    $t = " select *,
    (select countryName from tbl_country where tbl_country.country_id=(select country_id from tbl_address where tbl_address.address_id=tbl_proprietor.tmpAd_id)) as country,
   (select stateName from tbl_state where tbl_state.state_id=(select province_id from tbl_address where tbl_address.address_id=tbl_proprietor.tmpAd_id)) as state,
   (select districtName from tbl_district where tbl_district.district_id = (select district_id from tbl_address where tbl_address.address_id=tbl_proprietor.tmpAd_id)) as district,
   (select municipalityName from tbl_municipality where tbl_municipality.municipality_id = (select muncipality_id from tbl_address where tbl_address.address_id=tbl_proprietor.tmpAd_id)) as municipalityName,
   (select wardNo from tbl_address where tbl_address.address_id=tbl_proprietor.tmpAd_id) as wardNo,
   (select cityName from tbl_address where tbl_address.address_id=tbl_proprietor.tmpAd_id) as cityName,
   (select countryName from tbl_country where tbl_country.country_id=(select country_id from tbl_address where tbl_address.address_id=tbl_proprietor.permAd_id)) as pcountry,
   (select stateName from tbl_state where tbl_state.state_id=(select province_id from tbl_address where tbl_address.address_id=tbl_proprietor.permAd_id)) as pstate,
   (select districtName from tbl_district where tbl_district.district_id = (select district_id from tbl_address where tbl_address.address_id=tbl_proprietor.permAd_id)) as pdistrict,
   (select municipalityName from tbl_municipality where tbl_municipality.municipality_id = (select muncipality_id from tbl_address where tbl_address.address_id=tbl_proprietor.permAd_id)) as pmunicipalityName,
   (select wardNo from tbl_address where tbl_address.address_id=tbl_proprietor.permAd_id) as pwardNo,
   (select cityName from tbl_address where tbl_address.address_id=tbl_proprietor.permAd_id) as pcityName
    from  tbl_proprietor where proprietor_id='$proprietor_id'";
    $DataRow = BibDB::getRowByQuery($t);
    return ($DataRow) ? $DataRow : false;
}
function getPartners($industry_id)
{
    $t = "select partners from tbl_industry where industry_id='$industry_id'";
    $Partners = explode(",", BibDB::getRowByQuery($t)->partners);
    $IndustryPartners = array();
    foreach ($Partners as $Partner) {
        $IndustryPartners[] = getProprietor($Partner);
    }
    return ($IndustryPartners) ? $IndustryPartners : false;
}
function getApplicantCompiled($proprietor_id)
{
    $t = " select *,
    (select countryName from tbl_country where tbl_country.country_id=(select country_id from tbl_address where tbl_address.address_id=tbl_proprietor.tmpAd_id)) as country,
   (select stateName from tbl_state where tbl_state.state_id=(select province_id from tbl_address where tbl_address.address_id=tbl_proprietor.tmpAd_id)) as state,
   (select districtName from tbl_district where tbl_district.district_id = (select district_id from tbl_address where tbl_address.address_id=tbl_proprietor.tmpAd_id)) as district,
   (select municipalityName from tbl_municipality where tbl_municipality.municipality_id = (select muncipality_id from tbl_address where tbl_address.address_id=tbl_proprietor.tmpAd_id)) as municipalityName,
   (select wardNo from tbl_address where tbl_address.address_id=tbl_proprietor.tmpAd_id) as wardNo,
   (select cityName from tbl_address where tbl_address.address_id=tbl_proprietor.tmpAd_id) as cityName,
   (select countryName from tbl_country where tbl_country.country_id=(select country_id from tbl_address where tbl_address.address_id=tbl_proprietor.permAd_id)) as pcountry,
   (select stateName from tbl_state where tbl_state.state_id=(select province_id from tbl_address where tbl_address.address_id=tbl_proprietor.permAd_id)) as pstate,
   (select districtName from tbl_district where tbl_district.district_id = (select district_id from tbl_address where tbl_address.address_id=tbl_proprietor.permAd_id)) as pdistrict,
   (select municipalityName from tbl_municipality where tbl_municipality.municipality_id = (select muncipality_id from tbl_address where tbl_address.address_id=tbl_proprietor.permAd_id)) as pmunicipalityName,
   (select wardNo from tbl_address where tbl_address.address_id=tbl_proprietor.permAd_id) as pwardNo,
   (select cityName from tbl_address where tbl_address.address_id=tbl_proprietor.permAd_id) as pcityName
    from  tbl_proprietor where proprietor_id='$proprietor_id'";
    DB::select($t);
    $AddressRow = BibDB::getRowByQuery($t);
    //BibClass::pre($AddressRow);
    if ($AddressRow) {
        return $AddressRow;
    } else
        return false;
}
function getPropritorCompiled($proprietor_id)
{
    // $t="drop view view_proprietor"; DB::select($t);
    $t = "create or replace view view_proprietor
    as
    select *,
    (select countryName from tbl_country where tbl_country.country_id=(select country_id from tbl_address where tbl_address.address_id=tbl_proprietor.tmpAd_id)) as country,
   (select stateName from tbl_state where tbl_state.state_id=(select province_id from tbl_address where tbl_address.address_id=tbl_proprietor.tmpAd_id)) as state,
   (select districtName from tbl_district where tbl_district.district_id = (select district_id from tbl_address where tbl_address.address_id=tbl_proprietor.tmpAd_id)) as district,
   (select municipalityName from tbl_municipality where tbl_municipality.municipality_id = (select muncipality_id from tbl_address where tbl_address.address_id=tbl_proprietor.tmpAd_id)) as municipalityName,
   (select wardNo from tbl_address where tbl_address.address_id=tbl_proprietor.tmpAd_id) as wardNo,
   (select cityName from tbl_address where tbl_address.address_id=tbl_proprietor.tmpAd_id) as cityName,
   (select countryName from tbl_country where tbl_country.country_id=(select country_id from tbl_address where tbl_address.address_id=tbl_proprietor.permAd_id)) as pcountry,
   (select stateName from tbl_state where tbl_state.state_id=(select province_id from tbl_address where tbl_address.address_id=tbl_proprietor.permAd_id)) as pstate,
   (select districtName from tbl_district where tbl_district.district_id = (select district_id from tbl_address where tbl_address.address_id=tbl_proprietor.permAd_id)) as pdistrict,
   (select municipalityName from tbl_municipality where tbl_municipality.municipality_id = (select muncipality_id from tbl_address where tbl_address.address_id=tbl_proprietor.permAd_id)) as pmunicipalityName,
   (select wardNo from tbl_address where tbl_address.address_id=tbl_proprietor.permAd_id) as pwardNo,
   (select cityName from tbl_address where tbl_address.address_id=tbl_proprietor.permAd_id) as pcityName
    from  tbl_proprietor";
    DB::select($t);
    $t = "select * from view_proprietor where proprietor_id=$proprietor_id";
    //echo $t;die;

    $AddressRow = BibDB::getRowByQuery($t);
    //BibClass::pre($AddressRow);
    if ($AddressRow) {
        $AddressRow->Industry = getIndustryCompiled($AddressRow->industry_id);
        $AddressRow->PermanentAddress = getAddressCompiled($AddressRow->permAd_id);
        $AddressRow->TempAddress = getAddressCompiled($AddressRow->tmpAd_id);
        return $AddressRow;
    } else
        return false;
}
function getProductCompiled($product_id)
{
    $t = "select * from tbl_product where product_id='$product_id'";
    $DataRow = BibDB::getRowByQuery($t);
    if ($DataRow) {
        $DataRow->Industry = getIndustryCompiled($DataRow->industry_id);
        return $DataRow;
    } else
        return false;
}
function getRecommendationApplicationsCompiled()
{
}
function updateInPartners($industry_id, $partner_id)
{
    $Partners = explode(",", BibDB::lookupField("tbl_industry", "partners", "industry_id", $industry_id));
    $Partners[] = $partner_id;
    BibDB::updateRow($tableName = "tbl_industry", $fieldName = "partners", $fieldValue = implode(",", $Partners), $referenceField = "industry_id", $referenceValue = $industry_id);
}
function getLicensedProductsCompiled()
{
    $data = DB::where('tbl_licensedproducts.createdBy', auth()->user()->id)
        ->orderBy('created_at', 'desc')
        ->leftJoin('tbl_producttype', 'tbl_licensedproducts.producttype_id', '=', 'tbl_producttype.producttype_id')
        ->leftJoin('tbl_industry', 'tbl_licensedproducts.industry_id', '=', 'tbl_industry.industry_id')
        ->select('tbl_licensedproducts.*', 'tbl_producttype.productTypeName', 'tbl_industry.industryName')
        ->get();
    return $data;
}
function showIndustryInfo ($industry_id=0){
    $data=($industry_id)?getIndustryCompiled($industry_id):getIndustries()[0];
    return view("dftqc.ajaxForms.ajax-show-industry-info",['data'=>$data]);
}
function showIndustryDetailsForLicense($industry_id = 0)
{
    $data=($industry_id)?getIndustryCompiled($industry_id):getIndustries()[0];
    return view("dftqc.ajaxForms.ajax-show-industry-license-application",['data'=>$data]);
}
function sampleHelper()
{
    echo "COW";
}