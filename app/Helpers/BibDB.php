<?php
namespace App\Helpers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
class BibDB
{
    static function lookupField($table, $field, $refField, $refValue) 
    {
        $t="select $field from $table where $refField = '$refValue'";
        $Value=DB::select($t);
        
        if(!empty($Value))
        {
            return $Value[0]->$field;
        }
        else
        {
            return "Not Found in Table";
         }      


    }
    static function getRow($table,$condition="1")
    {
        $t="select * from $table where $condition";
        $Value=DB::select($t);
        return (empty($Value)?"Not Found":$Value[0]);
    }
    static function getRowByQuery($query)
    {
        $Value=DB::select($query);
        return (empty($Value)?false:$Value[0]);
    }
    static function getTableByQuery($query)
    {
        $Value=DB::select($query);
        return (empty($Value)?false:$Value);
    }
    static function updateRow($tableName,$fieldName,$fieldValue,$referenceField,$referenceValue)
    {
        $t="update $tableName set $fieldName='$fieldValue'  where $referenceField=$referenceValue";
       return  DB::select($t);
    }
}

