<?php

use App\Helpers\BibClass;
use App\Http\Controllers\NepaliDictonary\DictonaryController;
use App\Models\Hr\Employee;
use App\Models\Master\Country;
use App\Models\Master\District;
use App\Models\Master\Module;
use App\Models\Master\State;
use App\Models\Role;
use App\Models\Settings\OrganizationSettings;
use App\Models\Setting\Setting;
use App\Models\Settings\NotificationSettings;
use App\Models\Settings\UserSettings;
use App\Models\Log\OperationLog;
use App\Models\Log\ErrorLog;
use App\Models\Log\ActivityLog;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use App\Models\Master\Municipality;

function label($text)
{
    $text = str_replace("lang.", "", $text);
    if (App::getLocale() == "ne") {
        $nepaliWord = DB::scalar("select nepaliWord from tbl_dictonary where englishWord='$text'");
        if ($nepaliWord == "") {
            DictonaryController::injectEnglish($text);
            echo $text;
        } else {
            echo $nepaliWord;
        }
    } else {
        echo $text;
    }
}
function nullableFields($table)
{
    $columns = DB::select("describe $table");
    unset($columns[0]);
    foreach ($columns as $column) {
        if ($column->Type == "varchar(255)") {
            $q = "ALTER TABLE $table CHANGE $column->Field $column->Field VARCHAR(255) NULL DEFAULT NULL";
            //echo $q;die;
            DB::select($q);
        }
        if ($column->Type == "text") {
            $q = "ALTER TABLE $table CHANGE $column->Field $column->Field text NULL DEFAULT NULL";
            //echo $q;die;
            DB::select($q);
        }
        if ($column->Type == "int(11)") {
            $q = "ALTER TABLE $table CHANGE $column->Field $column->Field int(11) NULL DEFAULT NULL";
            //echo $q;die;
            DB::select($q);
        }
        if ($column->Type == "date") {
            $q = "ALTER TABLE $table CHANGE $column->Field $column->Field date NULL DEFAULT NULL";
            //echo $q;die;
            DB::select($q);
        }
    }
}
function createInput($type, $name, $id, $display, $class = "", $value = "", $placeHolder = "", $min = "")
{
?>
    <label for="<?php echo $id; ?>" class="form-label col-form-label <?php echo $class; ?>">
        <?php echo label($display); ?>
    </label>
    <input type="<?php echo $type; ?>" id="<?php echo $id; ?>" min="<?php echo $min; ?>" placeholder="<?php echo $placeHolder; ?>" name="<?php echo $name; ?>" class="form-control <?php $class; ?>" value="<?php echo $value ? $value : ''; ?>">
<?php
}
function createPassword($name, $id, $display, $class = "", $value = "", $placeHolder = "", $readonly = "")
{

?>
    <label for="<?php echo $id; ?>" class="form-label col-form-label"> <?php echo label($display); ?> </label>
    <div class="form-control-wrap">
        <input type="password" id="<?php echo $id; ?>" <?php echo $readonly; ?> placeholder="<?php echo $placeHolder; ?>" name="<?php echo $name; ?>" class="form-control <?php $class; ?>" value="<?php echo $value; ?>">
    </div>
<?php
}
function createEmail($name, $id, $display, $class = "", $value = "", $placeHolder = "", $readonly = "")
{
?>
    <label for="<?php echo $id; ?>" class="form-label col-form-label"> <?php echo label($display); ?> </label>
    <div class="form-control-wrap">
        <input type="email" id="<?php echo $id; ?>" <?php echo $readonly; ?> placeholder="<?php echo $placeHolder; ?>" name="<?php echo $name; ?>" class="form-control <?php $class; ?>" value="<?php echo $value; ?>">
    </div>
<?php
}
function createText($name, $id, $display, $class = "", $value = "", $placeHolder = "", $readonly = "")
{
?>
    <label for="<?php echo $id; ?>" class="form-label col-form-label"> <?php echo label($display); ?> </label>
    <div class="form-control-wrap">
        <input type="text" id="<?php echo $id; ?>" <?php echo $readonly; ?> placeholder="<?php echo $placeHolder; ?>" name="<?php echo $name; ?>" class="form-control <?php $class; ?>" value="<?php echo $value; ?>">
    </div>
    <p id='error_<?php echo $name; ?>' class='text-danger custom-error <?php echo $class; ?>'></p>
<?php
}
function createTextValidation($name, $id, $display, $class = "", $value = "", $placeHolder = "", $readonly = "", $required = "")
{

    // $add = trans('lang.Add');
?> <div class="wizard-form-error">* <?php echo label($display); ?> Requried</div>
    <label for="<?php echo $id; ?>" class="wizard-form-text-label"><?php echo label($display); ?></label>
    <input type="text" id="<?php echo $id; ?>" <?php echo $readonly; ?> placeholder="<?php echo $placeHolder; ?>" class="form-control <?php $class; ?>" name="<?php echo $name; ?>" value="<?php echo $value; ?>" <?php echo $required; ?>>
<?php
}

function createNumber($name, $id, $display, $class = "", $value = "", $placeHolder = "")
{

?>
    <label for="<?php echo $id; ?>" class="form-label col-form-label"> <?php echo label($display); ?> </label>
    <input type="number" id="<?php echo $id; ?>" placeholder="<?php echo $placeHolder; ?>" name="<?php echo $name; ?>" class="form-control <?php $class; ?>" value="<?php echo $value; ?>">
<?php
}

function createHidden($name, $id, $display, $class = "", $value = "", $placeHolder = "")
{
?>
    <label for="<?php echo $id; ?>" class="form-label col-form-label"> <?php echo label($display); ?> </label>
    <input type="hidden" id="<?php echo $id; ?>" placeholder="<?php echo $placeHolder; ?>" name="<?php echo $name; ?>" class="form-control <?php $class; ?>" value="<?php echo $value; ?>">
<?php
}

function createColor($name, $id, $display, $class = "", $value = "", $placeHolder = "")
{
?>
    <label for="<?php echo $id; ?>" class="form-label col-form-label"> <?php echo label($display); ?> </label>
    <input type="color" id="<?php echo $id; ?>" placeholder="<?php echo $placeHolder; ?>" name="<?php echo $name; ?>" class="form-control <?php $class; ?>" value="<?php echo $value; ?>">
<?php
}

function createDate($name, $id, $display, $class = "", $value = "", $placeHolder = "", $attribute = "")
{

?>
    <label for="<?php echo $id; ?>" class="form-label col-form-label"> <?php echo $display; ?> </label>
    <input type="date" id="<?php echo $id; ?>" placeholder="<?php echo $placeHolder; ?>" name="<?php echo $name; ?>" class="form-control <?php $class; ?>" value="<?php echo $value; ?>" <?php echo $attribute; ?>>
<?php
}
function createDateOne($name, $id, $display, $class = "", $value = "", $placeHolder = "", $attribute = "")
{

?>

    <div class="row ">
        <label for="colFormLabel <?php echo $id; ?>" class="col-sm-1 col-form-label"><?php echo $display; ?></label>
        <div class="col-sm-2">
            <div class="form-control-wrap">

                <input type="date" id="colFormLabel <?php echo $id; ?>" placeholder="<?php echo $placeHolder; ?>" name="<?php echo $name; ?>" class="form-control <?php $class; ?>" value="<?php echo $value; ?>" <?php echo $attribute; ?>>


            </div>
        </div>
    </div>
    <?php
}

function createRadio($name, $id, $class = "", $values = array(), $display = null)
{
    $sn = 0;
    for ($i = 0; $i < sizeof($values); $i++) : $v = $values[$i][0];
        $d = $values[$i];
        $d = $display ? $display : $d;
        $d = trans('lang.' . $d);
        $v = $values[$i];
        $sn++;
        if ($d != "") : ?> <div class="d-flex justify-content-left align-items-center form-check form-check-inline"> <input type="radio" id="<?php echo $id . $sn; ?>" name="<?php echo $name; ?>" class="form-check-input <?php $class; ?>" value="<?php echo $v; ?>"><label for="<?php echo $id . $sn; ?>" class="form-label col-form-label ms-2 mt-1"> <?php echo $d; ?> </label><?php endif; ?>
            </div>
            <?php endfor;
    }
    //Another Radio for non bolds
    function createRadio2($name, $id, $class = "", $values = array())
    {
        $sn = 0;
        for ($i = 0; $i < sizeof($values); $i++) : $v = $values[$i][0];
            $d = $values[$i][1];
            $sn++;
            if ($d != "") : ?> <div class="d-flex justify-content-left align-items-center form-check form-check-inline"> <input type="radio" id="<?php echo $id . $sn; ?>" name="<?php echo $name; ?>" class="form-check-input <?php $class; ?>" value="<?php echo $v; ?>"><label for="<?php echo $id . $sn; ?>" class="form-label col-form-label ms-2 mt-1"> <?php echo $d; ?> </label><?php endif; ?>
                </div>
            <?php endfor;
    }

    //Select Dropdown
    function createSelect($name, $id, $class = "", $display = "", $values = array())
    {
            ?>
            <select class="js-select <?php $class; ?>" name="<?php echo $name; ?>" aria-label="Default select example" data-search="true" data-sort="false">
                <option value="">Select Option</option>
                <?php $sn = 0;
                for ($i = 0; $i < sizeof($values); $i++) : $v = $values[$i][0];
                    $d = isset($values[$i]) ? $values[$i] : "";
                    $sn++; ?>

                    <option value="<?php echo $v; ?>"><?php echo ($d) ? $d : $v; ?></option>

                <?php endfor; ?>
            </select>
        <?php
    }
    function customCreateSelect($name, $id, $class = "form-control", $display, $values = array(), $keyValue = '')
    {
        $disabled = (in_array("DISABLED", explode(" ", strtoupper($class)))) ? "Disabled" : false;
        ?><label for="<?php echo $id; ?>" class="form-label col-form-label"> <?php echo label($display); ?> </label>
            <?php if ($disabled) : ?>
                <input type="hidden" name="<?php echo $name; ?>" value="<?php echo $keyValue; ?>" /><?php endif; ?>
                <select class="form-select <?php echo $class ?>" name="<?php echo $name; ?>" id="<?php echo $name; ?>" aria-label="Default select example" <?php echo ($disabled) ? "Disabled" : ""; ?>>
                    <option value=""><?php label("Select Option"); ?></option>
                    <?php foreach ($values as $key => $value) { ?>
                        <option value="<?= $key ?>" <?php echo $keyValue == $key ? 'selected' : '' ?>><?= $value ?></option>
                    <?php } ?>
                </select>

            <p id='error_<?php echo $name; ?>' class='text-danger custom-error <?php echo $class; ?>'></p>
        <?php
    }
    function createCheck($name, $id, $display, $class = "", $value = "", $placeHolder = "")
    {

        ?>
            <div class="form-check">
                <input type="checkbox" id="<?php echo $id; ?>" placeholder="<?php echo $placeHolder; ?>" name="<?php echo $name; ?>" class="form-check-input <?php $class; ?>" value="<?php echo $value; ?>">
                <label for="<?php echo $id; ?>" class="form-check-label"> <?php echo $display; ?> </label>
            </div>
        <?php
    }
    function createLabel($for = "", $class = "", $display)
    {

        ?>
            <label for="<?php echo $for; ?>" class="<?php echo $class; ?>">
                <?php echo label($display); ?>
            </label>
        <?php
    }
    function actionButton($name, $display = "", $url, $class = "", $extra = "")
    {
        ?><a href="<?php echo $url; ?>" name="<?php echo $name; ?>" data-title="<?php echo $name; ?>" type="button" class="btn btn-color-success btn-hover-success btn-icon btn-soft <?php echo $class; ?>" <?php echo $extra; ?>><?php echo $display; ?></a>
            <?php
        }
        function actionCanvasButton($name = "", $class = "", $dataTarget = "", $iconClass = "", $route = "", $id)
        {
            if ($iconClass == "edit") {
            ?>
                <button class="btn btn-color-primary btn-hover-primary btn-icon btn-soft <?php echo $class; ?>" name="<?php echo $name; ?>" data-route="<?php echo route($route, $id); ?>" data-bs-toggle="offcanvas" data-bs-target="#<?php echo $dataTarget; ?>"> <em class="icon ni ni-<?php echo $iconClass; ?>"></em></button>
            <?php
            } else {
            ?>
                <button class="btn btn-color-success btn-hover-success btn-icon btn-soft <?php echo $class; ?>" name="<?php echo $name; ?>" data-route="<?php echo route($route, $id); ?>" data-bs-toggle="offcanvas" data-bs-target="#<?php echo $dataTarget; ?>"> <em class="icon ni ni-<?php echo $iconClass; ?>"></em></button>

            <?php
            }
        }
        function deleteCanvasButton($name = "", $class = "btn-hover-danger", $route = "", $id)
        {
            ?>
            <button type="button" data-route="<?php echo route($route, $id); ?>" class="btn btn-color-danger <?php echo $class; ?> btn-icon btn-soft"><em class="icon ni ni-trash"></em></button>
        <?php
        }
        function createButton($class = "", $type = "submit", $display = "Submit")
        {

        ?>
            <button class="btn btn-primary <?php echo $class; ?>" type="<?php echo $type; ?>">
                <?php echo $display ?>
            </button>
        <?php
        }
        function btnForm($display = "Submit", $type = "submit", $class = "")
        {

        ?>
            <button class="mt-3 btn btn-primary <?php echo $class; ?>" type="<?php echo $type; ?>">
                <?php echo $display ?>
            </button>
            <?php
        }
        function createCanvasButton($class = "", $type = "", $display, $route)
        {

            $add = trans('lang.Add');
            if (App::getLocale() == 'ne') {
            ?>
                <button class="mt-3 btn btn-primary <?php echo $class; ?>" data-route="<?php echo route($route); ?>" data-bs-toggle="offcanvas" data-bs-target="#addOffcanvas"> <em class="icon ni ni-plus"></em> <?php echo $display ?> <?php echo $add ?>
                </button>
            <?php
            } else {
            ?>
                <button class="mt-3 btn btn-primary <?php echo $class; ?>" data-route="<?php echo route($route); ?>" data-bs-toggle="offcanvas" data-bs-target="#addOffcanvas"> <em class="icon ni ni-plus"></em><?php echo $add ?> <?php echo $display ?>
                </button>
            <?php
            }
        }
        function createTextArea($name, $class = "", $id = "", $row = "", $display = "")
        {
            ?>
            <div class="form-group">
                <div class="form-control-wrap">
                    <textarea class="form-control text-area" name="<?php echo $name; ?>" id="<?php echo $id; ?>" rows="<?php echo $row; ?>"><?php if (isset($display)) {
                                                                                                                                                echo strip_tags($display);
                                                                                                                                            } ?></textarea>
                </div>
            </div>

        <?php
        }
        function inputwithbottommargin($type, $name, $id, $display, $class = "", $value = "", $placeHolder = "")
        {
        ?>
            <input type="<?php echo $type; ?>" id="<?php echo $id; ?>" placeholder="<?php echo $placeHolder; ?>" name="<?php echo $name; ?>" class="form-control mb-2" value="<?php echo $value; ?>">
        <?php
        }
        function textInput($name, $id, $display = "", $value = "", $class = "")
        {
        ?>
            <input type="text" id="<?php echo $id; ?>" placeholder="<?php echo ($display) ? $display : ""; ?>" name="<?php echo $name; ?>" class="<?php echo $class; ?>" value="<?php echo $value; ?>">
        <?php
        }

        function master_getColumn($tableName)
        {
            return Schema::getColumnListing($tableName);
        }

        function master_storeColumn($tableName, $data)
        {
            $allcolumns = Schema::getColumnListing($tableName);

            $datakey = array_keys($data);
            // dd($allcolumns, $data);
            foreach ($data as $key => $value) {
                if (in_array($key, $allcolumns)) {
                    DB::table($tableName)->insert($data);
                    return true;
                }
            }
        }

        function master_updateColumn($tableName, $data, $id)
        {
            $TablePK = mid($tableName, 4) . "_id";
            $allcolumns = Schema::getColumnListing($tableName);

            $datakey = array_keys($data);
            // dd($allcolumns, $data);
            foreach ($data as $key => $value) {
                if (in_array($key, $allcolumns)) {
                    DB::table($tableName)->where($tablePK, $id)->update($data);
                    return true;
                }
            }
        }

        function getForeignTable($all_columns)
        {
            $string = '_id';
            $foreign = [];
            foreach ($all_columns as $key => $column) {
                if (str_contains($column, $string) !== FALSE) { // Yoshi version
                    $foreign[] = $column;
                }
            }
            unset($foreign[0]);
            $strArray = [];
            foreach ($foreign as $key => $foreignKey) {
                $strArray[$key] = explode('_id', $foreignKey);
                unset($strArray[$key][1]);
            }
            if ($strArray) {
                $all_Foreign_Key_Table = call_user_func_array('array_merge', $strArray);
                foreach ($all_Foreign_Key_Table as $column) {
                    $tableName[] = "tbl_" . $column;
                }
                return $tableName;
            } else {
                return [];
            }
        }

        /**
         * $tableName = Name of table
         * $pk = primary key of table
         * $name = table select column name
         * $class = extra class
         * $data = Existing data or for edit case showing selected data
         * $display = Displaying name or showing label name.
         */

        function getSelectForForeignColumn($tableName = "", $pk, $name, $class = "form-control", $data = null, $display = null)
        {
            $systems = DB::table($tableName)->where('status', '<>', -1)->orderBy($pk, 'asc')->pluck($name, $pk);
            customCreateSelect($pk, $pk, $class, $display ?? $name, $systems, ($data) ? $data->$pk : null);
        }




        function usersetting($query)
        {
            $usersetting = UserSettings::fetch($query)->where('user_id', auth()->user()->id)->first();
            return $usersetting ? $usersetting->value : null;
        }

        function organizationSetting($query)
        {
            $organizationSetting = OrganizationSettings::fetch($query)->first();
            return $organizationSetting ? $organizationSetting->value : null;
        }

        function notificationSetting($query)
        {
            $notificationSetting = NotificationSettings::fetch($query)->first();
            return $notificationSetting ? $notificationSetting->value : null;
        }

        function includeHelper()
        {
        }

        function getAjaxCreateModalContent($controllerName)
        {
            return "App\http\Controllers\\$controllerName"::getAjaxContent('create');
        }

        //get Countries
        function getCountries()
        {
            return Country::getCountries();
        }

        function getStates()
        {
            return State::getStates();
        }
        function getStatesByCountryId($country_id)
        {
            return State::getStatesByCountryId($country_id);
        }
        function getDistricts()
        {
            return District::getDistricts();
        }

        function getDistrictsByStateId($state_id)
        {
            return District::getDistrictsByStateId($state_id);
        }

        function getMunicipalities()
        {
            return Municipality::getMunicipalities();
        }

        function getMunicipalityByDistrictId($district_id)
        {
            return Municipality::getMunicipalitiesget($district_id);
        }

        function slugify($text, string $divider = '-')
        {
            // replace non letter or digits by divider
            $text = preg_replace('~[^\pL\d]+~u', $divider, $text);

            // transliterate
            // $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

            // remove unwanted characters
            $text = preg_replace('~[^-\w]+~', '', $text);

            // trim
            $text = trim($text, $divider);

            // remove duplicate divider
            $text = preg_replace('~-+~', $divider, $text);

            // lowercase
            $text = strtolower($text);

            if (empty($text)) {
                return 'n-a';
            }

            return $text;
        }

        function getRoles()
        {
            return Role::whereNotIn('id', [1])->get();
        }
        function getEmployees()
        {
            return Employee::where('status', '<>', '-1')->get();
        }

        function getModules()
        {
            return Module::where('status', '<>', '-1')->where('module_id', '<>', '1')->get();
        }

        function getReportingTo($department_id)
        {
            return Employee::where('status', '<>', -1)->where('department_id', $department_id)->where('is_head', 'manager')->orderBy('created_at', 'desc')->get();
        }

        function getEmployeeHasNoLogin()
        {
            return Employee::where('status', '<>', -1)->where('is_login', 0)->get();
        }

        //get unique Operation number
        function getOperationNumber()
        {
            $startNumber = date('YmdHis') . rand(1000, 9999);
            $isExists = OperationLog::where('operation_end_no', $startNumber)->first();
            while ($isExists) {
                $startNumber = date('YmdHis') . rand(1000, 9999);
                $isExists = OperationLog::where('operation_end_no', $startNumber)->first();
            }
            return $startNumber;
        }

        /**
         * function createLog(operation start number, operation end number, model class full name with path,model Id for create and upodate operation, operation Name, previous values in array, new values in array);
         */
        function createOperationLog($startOperationNumber, $endOperationNumber, $modelName, $modelId, $operationName, $previousValues, $newValues)
        {
            $operationId = getOperationNumber();
            $user_id = auth()->user()->id;
            OperationLog::create([
                'user_id' => $user_id,
                'operation_start_no' => $startOperationNumber,
                'operation_end_no' => $endOperationNumber,
                'model_name' => $modelName,
                'model_id' => $modelId,
                'operation_name' => $operationName,
                'previous_values' => $previousValues ? json_encode($previousValues) : null,
                'new_values' => $newValues ? json_encode($newValues) : null,
            ]);
        }

        function createErrorLog($controllerName, $methodName, $errors)
        {
            $user_id = auth()->user()->id;
            ErrorLog::create([
                'user_id' => $user_id,
                'controller_name' => $controllerName,
                'method_name' => $methodName,
                'errors' => $errors,
            ]);
        }

        function createErrorParagraph($name, $class = null)
        {
            echo "<p id='error_$name' class='text-danger custom-error $class'></p>";
        }

        function createActivityLog($controllerName, $methodName, $activity)
        {
            $user_id = auth()->user()->id;
            ActivityLog::create([
                'user_id' => $user_id,
                'controllerName' => $controllerName,
                'methodName' => $methodName,
                'actionUrl' => request()->fullUrl(),
                'activity' => $activity,
            ]);
        }


        //function by bishwas get all data from tbl_licensingrequireddocuments table

        // function getDocumentNamesListFromLicensingRequiredDocuments()
        //         {
        //     $documentNames = DB::table('tbl_licensingrequireddocuments')->pluck('documentName');
        //             $list = '<ul class = "list-group">';

        //             foreach ($documentNames as $documentName) {
        //                 $list .= '<li class="p-2">' . $documentName . '</li>';
        //             }

        //             $list .= '</ul>';

        //             return $list;
        //         }

        function getFielddatafromtable($tablename, $fieldname,)
        {
            $documentNames = DB::table($tablename)->pluck($fieldname);
            $list = '<ul class = "list-group list-group-flush">';

            foreach ($documentNames as $documentName) {
                $list .= '<li class="list-group-item" >' . $documentName . '</li>';
            }

            $list .= '</ul>';

            return $list;
        }

        function uploadOrReplaceSingleFile($file,$fileName,$location='',$path='',$document_id = null, $existingPath = null, $disk='public')
        {
            //Delete Existing path
            $document = null;
            if($document_id){
                $document = DB::table('tbl_uploadeddocuments')->where('id',$document_id)->first();
                if($document){
                    $existingPath = $document->documentPath;
                    if (Storage::disk($disk)->exist($existingPath)) {
                        Storage::disk($disk)->delete($existingPath);
                    }
                }
            }else if($existingPath){
                if (Storage::disk($disk)->exist($existingPath)) {
                    Storage::disk($disk)->delete($existingPath);
                }
            }

            $rand = mt_rand(1, 9999);
            $originalFileName = $file->getClientOriginalName();
                $originalFileName = str_replace(' ', '-', $originalFileName);
                $filename = $path . $rand . uniqid() . $originalFileName;
                $extension = $file->getClientOriginalExtension();
                // save to storage/app/photos as the new $filename
                $path = $file->storeAs($location, $filename, $disk);

                $imageExtension = ['jpg','JPG','jpeg','JPEG','png','PNG','GIF','gif'];
                $pdfExtension = ['pdf','PDF'];
                $documentArr = [];
                $documentStd = new stdClass;

                if(in_array($extension, $imageExtension)){
                    $documentArr['documentType'] = 'Image';
                }elseif(in_array($extension, $pdfExtension)){
                    $documentArr['documentType'] = 'PDF';
                }else{
                    $documentArr['documentType'] = 'Docs';
                }
                $documentArr['documentPath'] = $path;
                $documentArr['documentName'] = $fileName;
                $documentArr['documentBasePath'] = url('/').'/storage/';
                $documentArr['createdBy'] = auth()->user()->id;


                $documentStd->documentType = $documentArr['documentType'];
                $documentStd->documentPath = $documentArr['documentPath'];
                $documentStd->documentName = $documentArr['documentName'];
                $documentStd->documentBasePath = $documentArr['documentBasePath'];
                //if document is exists then update ther wise create.
                if($document){
                    $document->update($documentArr);
                    $documentStd->document_id = $document_id;
                }else{
                    $document_id = DB::table('tbl_uploadeddocuments')->insertGetId($documentArr);
                    $documentStd->document_id = $document_id;
                }

                return $documentStd;
        }//end of file

        function uploadFilesFromRepeator($documents,$documentNames,$location)
        {
            $uploadedDocuments = [];

            foreach($documents as $key=>$file){
                $docClass = uploadOrReplaceSingleFile($file,$documentNames[$key],$location);
                $uploadedDocuments['documents'][] = $docClass;
                $uploadedDocuments['ids'][] = $docClass->document_id;
            }
           return $uploadedDocuments;
        }
        // File Upload
        function uploadCommonFile($location, $file, $path, $existingPath = null, $disk='public') //default disk to upload file.
        {
            $rand = mt_rand(1, 9999);
            if ($file) {
                if ($existingPath) {
                    if (Storage::disk($disk)->exist($existingPath)) {
                        Storage::disk($disk)->delete($existingPath);
                    }
                }
                // generate a new filename. getClientOriginalExtension() for the file extension
                $originalFileName = $file->getClientOriginalName();
                $originalFileName = str_replace(' ', '-', $originalFileName);
                $filename = $path . $rand . time() . $originalFileName .'.' . $file->getClientOriginalExtension();
                $extension = $file->getClientOriginalExtension();
                // save to storage/app/photos as the new $filename
                $path = $file->storeAs($location, $filename, $disk);

                $imageExtension = ['jpg', 'JPG', 'jpeg', 'JPEG', 'png', 'PNG', 'GIF', 'gif'];
                $pdfExtension = ['pdf', 'PDF'];
                $documentArr = [];
                if (in_array($extension, $imageExtension)) {
                    $documentArr['documentType'] = 'Image';
                } elseif (in_array($extension, $pdfExtension)) {
                    $documentArr['documentType'] = 'PDF';
                } else {
                    $documentArr['documentType'] = 'Docs';
                }
                $documentArr['documentPath'] = $path;
                return $documentArr;
            }
        }

        //This is new function for upload document with its type
        function uploadMultipleDocumentsWithNames($DocumentFiles,$documentNames)
        {
            if (!empty($DocumentFiles)) {
                $documentsPathDetails = [];
                foreach ($DocumentFiles as $key => $value) {
                    $documentsPathDetails[$key] = uploadCommonFile('recommendation', $value, '');
                    $documentsPathDetails[$key]['documentName'] = $documentNames[$key];
                }
                $uploadedDocuments = [];
                foreach ($documentsPathDetails as $documentDel) {
                    $uploadedDocuments[] = DB::table('tbl_uploadeddocuments')->insertGetId($documentDel);
                }
                $uploadedDocuments = implode(",", $uploadedDocuments);
                return $uploadedDocuments;
            }
        }

        function UploadDocuments($DocumentFields)
        {
            $documentsPath = [];
            if (!empty($DocumentFields)) {
                foreach ($DocumentFields as $value) {
                    $documentsPath[] = uploadCommonFile('recommendation', $value, '');
                }
            }
            $data = $DocumentFields;
            $uploadedDocuments = array();

            if (!empty($data['document_name'])) {
                foreach ($data['document_name'] as $key => $value) {
                    if ($value != null) {
                        $files = [
                            'documentName' => $data['document_name'][$key],
                            'documentType' => null, //TO BE DETERMIND LATER USING UPLOADER FUNCTION
                            'documentPath' => $documentsPath[$key] ?? null,
                        ];
                        $uploadedDocuments[] = DB::table('users')->insertGetId($files);
                    }
                }
            }
            $uploadedDocuments = implode(",", $uploadedDocuments);
            return $uploadedDocuments;
        }

        function correspondingNepaliNumber($num)
        {
            switch ($num) {
                case 0:
                    return '०';
                    break;
                case 1:
                    return '१';
                    break;
                case 2:
                    return '२';
                    break;
                case 3:
                    return '३';
                    break;
                case 4:
                    return '४';
                    break;
                case 5:
                    return '५';
                    break;
                case 6:
                    return '६';
                    break;
                case 7:
                    return '७';
                    break;
                case 8:
                    return '८';
                    break;
                case 9:
                    return '९';
                    break;
                default:
                    return '';
            }
        }
        function getNepaliNumber($number)
        {
            $number = str_replace(' ', '', $number);
            if (isset($number) && is_numeric($number)) {
                $number = str_split($number);
                $nepaliNumber = '';
                foreach ($number as $num) {
                    $nepaliNumber .= correspondingNepaliNumber($num);
                }
                return $nepaliNumber;
            } else {
                return false;
            }
        }
        function convertNumAccToLang($num)
        {
            if (App::getLocale() == 'ne') {
                return getNepaliNumber($num);
            } else {
                return $num;
            }
        }
        function createUploadRepeater($label="")
        {
        ?>
            <div class="card mt-4">
                <div class="card-header  bg-dark-primary p-2 text-white ">
                    <h4>
                        <?php ($label)? label ($label) : label('File Upload') ?>
                    </h4>
                </div>
                <div class="card-body">
                    <div id="addfilerepeater" class="pt-2">

                        <div class="form-group row d-flex align-items-end">
                            <div class="col-sm-4">
                                <label class="control-label"><?php label('File Name') ?></label>
                                <input type="text" name="document_name[]" class="form-control" required>
                            </div>

                            <div class="col-sm-6">
                                <label class="control-label"><?php label('Documents Upload') ?></label>
                                <input type="file" name="uploaded_documents[]" class="form-control" required>
                            </div>

                            <div class="col-md-2" style="margin-top: 45px;">
                                <input id="addfilerow" type="button" class="btn btn-primary mr-1" value="+ Add Row">
                            </div>
                        </div>

                        <input type="hidden" id="addfile" value="0" name="addfile">
                    </div>
                </div>
            </div>



            <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.3/jquery.min.js" integrity="sha512-STof4xm1wgkfm7heWqFJVn58Hm3EtS31XFaagaa8VMReCXAkQnJZ+jEy8PCC/iT18dFy95WcExNHFTqLyp72eQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
            <script>
                // For File Repeater
                $(document).on('click', '#addfilerow', function() {
                    var b = parseFloat($("#addfile").val());
                    b = b + 1;
                    $("#addfile").val(b);
                    var temp = $("#addfile").val();
                    var tst = "<div class='form-group row d-flex align-items-end appended-row'><div class='col-sm-4'><label class='control-label'><?php label('File Name'); ?></label>";
                    tst = tst + "<input type='text' name='document_name[]' class='form-control' required></div>";
                    tst = tst + "<div class='col-sm-6'><label class='control-label'><?php label('Documents Upload'); ?></label><input type='file' name='uploaded_documents[]' class='form-control' required></div>";
                    tst = tst + "<div class='col-md-2' style='margin-top: 45px;'><input class='removeitemrow btn btn-danger mr-1' type='button' value='<?php label("Remove"); ?>'></div></div>";
                    $('#addfilerepeater').append(tst);
                });

                $(document).on('click', '.removeitemrow', function() {
                    $(this).closest('.appended-row').remove();
                })

                $(document).on('click', '.descremoveitemrow', function() {
                    $(this).closest('.descappended-row').remove();
                })
            </script>
        <?php
        }
        function createUploaderRepeaterWithCustomName($fileName,$documentName){
            ?>
            <div class="card mt-4">
                <div class="card-header  bg-dark-primary p-2 text-white ">
                    <h4>
                        <?php label('File Upload') ?>
                    </h4>
                </div>
                <div class="card-body">
                    <div id="addfilerepeater" class="pt-2">

                        <div class="form-group row d-flex align-items-end">
                            <div class="col-sm-4">
                                <label class="control-label"><?php label('File Name') ?></label>
                                <input type="text" name="document_name[]" class="form-control" required>
                            </div>

                            <div class="col-sm-6">
                                <label class="control-label"><?php label('Documents Upload') ?></label>
                                <input type="file" name="uploaded_documents[]" class="form-control" required>
                            </div>

                            <div class="col-md-2" style="margin-top: 45px;">
                                <input id="addfilerow" type="button" class="btn btn-primary mr-1" value="+ Add Row">
                            </div>
                        </div>

                        <input type="hidden" id="addfile" value="0" name="addfile">
                    </div>
                </div>
            </div>



            <script>
                // For File Repeater
                $(document).on('click', '#addfilerow', function() {
                    var b = parseFloat($("#addfile").val());
                    b = b + 1;
                    $("#addfile").val(b);
                    var temp = $("#addfile").val();
                    var tst = "<div class='form-group row d-flex align-items-end appended-row'><div class='col-sm-4'><label class='control-label'><?php label('File Name'); ?></label>";
                    tst = tst + "<input type='text' name='document_name[]' class='form-control' required></div>";
                    tst = tst + "<div class='col-sm-6'><label class='control-label'><?php label('Documents Upload'); ?></label><input type='file' name='uploaded_documents[]' class='form-control' required></div>";
                    tst = tst + "<div class='col-md-2' style='margin-top: 45px;'><input class='removeitemrow btn btn-danger mr-1' type='button' value='<?php label("Remove"); ?>'></div></div>";
                    $('#addfilerepeater').append(tst);
                });

                $(document).on('click', '.removeitemrow', function() {
                    $(this).closest('.appended-row').remove();
                })

                $(document).on('click', '.descremoveitemrow', function() {
                    $(this).closest('.descappended-row').remove();
                })
            </script>
        <?php
        }
